﻿using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Xamarin.Forms;
using System.Net.Http;
using QuotesApp.Models;
using QuotesApp.Views;

namespace QuotesApp
{
    public partial class MainPage : ContentPage
    {
        public int Count = 0;
        private const string Url = "https://gist.githubusercontent.com/b1nary/ea8fff806095bcedacce/raw/6e6de20d7514b93dd69b149289264997b49459dd/enterpreneur-quotes.json";
        // This handles the Web data request
        private HttpClient _client = new HttpClient();
        
        public MainPage()
        {
            InitializeComponent();
            Init();
            OnGetList();
        }

        void Init()
        {
            BackgroundColor = Constants.BackgroudColor;
            label_hello.FontFamily = Device.RuntimePlatform == Device.iOS ? "Lobster-Regular" : "Lobster-Regular.ttf#Lobster-Regular";
        }

        protected async void OnGetList()
        {
            if (CrossConnectivity.Current.IsConnected)
            {

                try
                {
                    //Activity indicator visibility on
                    activity_indicator.IsRunning = true;
                    //Getting JSON data from the Web
                    var content = await _client.GetStringAsync(Url);
                    
                    //We deserialize the JSON data from this line
                    var tr = JsonConvert.DeserializeObject<List<MyItems>>(content).GetRange(0,100);
                    //After deserializing , we store our data in the List called ObservableCollection
                    ObservableCollection<MyItems> trends = new ObservableCollection<MyItems>(tr);
                    
                    myList.ItemsSource = trends;

                    myList.ItemTapped += MyList_ItemTapped;

                    Debug.Write(tr[0]);
                    //We check the number of Items in the Observable Collection
                    int i = trends.Count;
                    if (i > 0)
                    {
                        //If they are greater than Zero we stop the activity indicator
                        activity_indicator.IsRunning = false;
                    }


                }
                catch (Exception ey)
                {
                    Debug.WriteLine("" + ey);
                }

            }

        }

        private void MyList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            MyItems item = (MyItems)e.Item;
            Navigation.PushAsync(new DetailPage(item));
        }
    }
}
