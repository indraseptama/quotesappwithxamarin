﻿using QuotesApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuotesApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailPage : ContentPage
	{
		public DetailPage (MyItems item)
		{
			InitializeComponent ();
            itemDetailText.Text = item.text;
            itemDetailText.TextColor = Constants.MainText;
            itemAuthor.Text = item.from;
            itemAuthor.TextColor = Constants.MainText;
            BackgroundColor = Constants.BackgroudColor;
		}
	}
}